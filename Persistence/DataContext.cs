﻿using Domain;
using Microsoft.EntityFrameworkCore;

namespace Persistence
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions options) : base(options)
        {
            
        }

        public DbSet<Category> Categories { get; set; }
        public DbSet<CategoryField> CategoryFields { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductFieldValue> ProductFieldValues { get; set; }
        
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<CategoryField>()
                .HasKey(cf => cf.FieldId);
            
            builder.Entity<ProductFieldValue>()
                .HasKey(cf => cf.ValueId);
            
            builder.Entity<ProductFieldValue>()
                .HasOne(pfv => pfv.Product)
                .WithMany()
                .HasForeignKey(pfv => pfv.ProductId)
                .OnDelete(DeleteBehavior.NoAction); // Установка ON DELETE NO ACTION
            
            // Определение связи между Category и CategoryField
            builder.Entity<Category>()
                .HasMany(c => c.CategoryFields)
                .WithOne(cf => cf.Category)
                .HasForeignKey(cf => cf.CategoryId);

            // Определение связи между Product и Category
            builder.Entity<Product>()
                .HasOne(p => p.Category)
                .WithMany(c => c.Products)
                .HasForeignKey(p => p.CategoryId);

            // Определение связи между Product и ProductFieldValue
            builder.Entity<Product>()
                .HasMany(p => p.ProductFieldValues)
                .WithOne(pfv => pfv.Product)
                .HasForeignKey(pfv => pfv.ProductId);

            // Определение связи между CategoryField и ProductFieldValue
            builder.Entity<CategoryField>()
                .HasMany(cf => cf.ProductFieldValues)
                .WithOne(pfv => pfv.Field)
                .HasForeignKey(pfv => pfv.FieldId);
        }
    }
}