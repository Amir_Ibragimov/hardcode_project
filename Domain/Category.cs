﻿using System;
using System.Collections.Generic;

namespace Domain
{
    public class Category
    {
        public Guid CategoryId { get; set; }
        public string CategoryName { get; set; }
    
        // Связь с дополнительными полями
        public ICollection<CategoryField> CategoryFields { get; set; }

        // Связь с товарами
        public ICollection<Product> Products { get; set; }
    }
}