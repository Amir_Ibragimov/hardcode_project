﻿using System;
using System.Collections.Generic;

namespace Domain
{
    public class CategoryField
    {
        public Guid FieldId { get; set; }
        public Guid CategoryId { get; set; }
        public string FieldName { get; set; }
        public string FieldType { get; set; }

        // Связь с категорией
        public Category Category { get; set; }
        
        public ICollection<ProductFieldValue> ProductFieldValues { get; set; }
    }

}