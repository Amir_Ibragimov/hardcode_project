﻿using System;
using System.Collections.Generic;

namespace Domain
{
    public class Product
    {
        public Guid ProductId { get; set; }
        public string ProductName { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public Guid CategoryId { get; set; }

        // Связь с категорией
        public Category Category { get; set; }

        // Связь с динамическими полями
        public ICollection<ProductFieldValue> ProductFieldValues { get; set; }
    }
}