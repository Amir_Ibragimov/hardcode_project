﻿using System;

namespace Domain
{
    public class ProductFieldValue
    {
        public Guid ValueId { get; set; }
        public Guid ProductId { get; set; }
        public Guid FieldId { get; set; }
        public string FieldValue { get; set; }

        // Связь с товаром
        public Product Product { get; set; }

        // Связь с дополнительным полем категории
        public CategoryField Field { get; set; }
    }

}