﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Application.Core;
using MediatR;
using Persistence;

namespace Application.Products
{
    public class DeleteCategory
    {
        public class Command : IRequest<Result<Unit>>
        {
            public Guid IGuid { get; set; }
        }
        
        public class Handler : IRequestHandler<Command, Result<Unit>>
        {
            
            private readonly DataContext _context;

            public Handler(DataContext context)
            {
                _context = context;
            }
            
            public async Task<Result<Unit>> Handle(Command request, CancellationToken cancellationToken)
            {
                var category = await _context.Categories.FindAsync(request.IGuid);

                /*if (activity == null)
               {
                   return null;
               }*/
                _context.Remove(category);

                var result = await _context.SaveChangesAsync() > 0;

                if (!result)
                {
                    return Result<Unit>.Failure("Failed to delete the category");
                }

                return Result<Unit>.Success(Unit.Value);
            }
        }
    }
}