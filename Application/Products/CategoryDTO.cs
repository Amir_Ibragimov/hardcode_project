﻿namespace Application.Products
{
    public class CategoryDTO
    {
        public string CategoryName { get; set; }
    }
}