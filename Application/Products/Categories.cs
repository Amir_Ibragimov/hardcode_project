﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Core;
using Domain;
using MediatR;
using Persistence;

namespace Application.Products
{
    public class Categories
    {
        public class Query : IRequest<Result<List<Category>>>
        {
            
        }
        
        public class Handler : IRequestHandler<Query, Result<List<Category>>>
        {
            private readonly DataContext _context;

            public Handler(DataContext context)
            {
                _context = context;
            }
            
            public async Task<Result<List<Category>>> Handle(Query request, CancellationToken cancellationToken)
            {
                var categories = _context.Categories.ToList();
    
                return Result<List<Category>>.Success(categories);
            }
        }
    }
}