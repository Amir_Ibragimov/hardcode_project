﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Core;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.Products
{
    public class ProductDynamic
    {
        public class Query : IRequest<Result<List<ProductDTO>>>
        {
            
        }

        public class Handler : IRequestHandler<Query, Result<List<ProductDTO>>>
        {
            private readonly DataContext _context;

            public Handler(DataContext context)
            {
                _context = context;
            }
            
            public async Task<Result<List<ProductDTO>>> Handle(Query request, CancellationToken cancellationToken)
            {
                var productsWithFields = _context.Products
                    .Include(p => p.Category)
                    .SelectMany(p => p.ProductFieldValues.Select(pf => new ProductDTO
                    {
                        ProductId = p.ProductId,
                        ProductName = p.ProductName,
                        Description = p.Description,
                        Price = p.Price,
                        CategoryName = p.Category.CategoryName,
                        FieldName = pf.Field.FieldName,
                        FieldValue = pf.FieldValue
                    }))
                    .ToListAsync();
    
                return Result<List<ProductDTO>>.Success(await productsWithFields);
            }
        }
    }
}