﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Application.Core;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.Products
{
    public class CreateProduct
    {
        public class Command : IRequest<Result<Unit>>
        {
            public Guid IGuid { get; set; }
            public Product Product { get; set; }
        }

        public class Handler : IRequestHandler<Command, Result<Unit>>
        {
            private readonly DataContext _context;

            public Handler(DataContext context)
            {
                _context = context;
            }
            
            public async Task<Result<Unit>> Handle(Command request, CancellationToken cancellationToken)
            {
                var category = await _context.Categories.FirstOrDefaultAsync(x => 
                    x.CategoryId == request.IGuid);

                var product = new Product
                {
                    ProductName = request.Product.ProductName,
                    Description = request.Product.Description,
                    Price = request.Product.Price,
                    Category = category,
                };

                _context.Products.Add(product);

                var result = await _context.SaveChangesAsync() > 0;

                if (!result)
                {
                    return Result<Unit>.Failure("Failed to create category");
                }
                
                return Result<Unit>.Success(Unit.Value);
            }
        }
    }
}