﻿using System;
using System.Threading.Tasks;
using Application.Products;
using Domain;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    public class ProductController : BaseController
    {
        [HttpGet("{IGuid}")]
        public async Task<ActionResult> GetProductById(Guid IGuid)
        {
            return HandleResult(await Mediator.Send(new Details.Query{Product = IGuid}));
        }
        
        [HttpGet]
        public async Task<ActionResult> GetProducts()
        {
            return HandleResult(await Mediator.Send(new ProductDynamic.Query()));
        }
        
        [HttpPost]
        public async Task<ActionResult> CreateProduct([FromQuery]Guid IGuid, [FromBody]Product product)
        {
            return HandleResult(await Mediator.Send(new CreateProduct.Command{IGuid = IGuid, Product = product}));
        }
    }
}