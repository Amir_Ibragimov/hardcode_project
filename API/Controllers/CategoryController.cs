﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Application.Products;
using Domain;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    public class CategoryController : BaseController
    {
        [HttpGet]
        public async Task<ActionResult> GetCategories()
        {
            return HandleResult(await Mediator.Send(new Categories.Query()));
        }
        
        [HttpPost]
        public async Task<ActionResult> CreateCategory(Category category)
        {
            return HandleResult(await Mediator.Send(new CreateCategory.Command{Category = category}));
        }
        
        [HttpDelete]
        public async Task<ActionResult> DeleteCategory(Guid IGuid)
        {
            return HandleResult(await Mediator.Send(new DeleteCategory.Command{IGuid = IGuid}));
        }
    }
}